// Tutorial by http://youtube.com/CodeExplained
// api key : 82005d27a116c2880c8f0fcb866998a0
//1f3f7d7792msh039ca8b03c7ed1dp12c890jsncfb0bbcf2e95 moje konto
// Select Elements
//e72998cc037eea53b1e3006a17ace6ee

const iconElement = document.querySelector(".weather-icon");
const tempElement = document.querySelector(".temperature-value p");
const descElement = document.querySelector(".temperature-description p");
const locationElement = document.querySelector(".location p");
const notificationElement = document.querySelector(".notification");
const pressureElement = document.querySelector(".pressure");
const humidityElement = document.querySelector(".humidity");
const windSpeedElement = document.querySelector(".windSpeed");
const windDirElement = document.querySelector(".windDir");
const sunriseElement = document.querySelector(".sunrise");
const sunsetElement = document.querySelector(".sunset");
//stałe do przeliczenia czasu wschodu Słońca
const spanH = document.querySelector('span.h');
const spanM = document.querySelector('span.m');
const spanS = document.querySelector('span.s');
//stałe do przeliczenia czasu zachodu Słońca
const spanH1 = document.querySelector('span.h1');
const spanM1 = document.querySelector('span.m1');
const spanS1 = document.querySelector('span.s1');



// app data
const weather = {};

weather.temperature = {
    unit: "celsius"
} // App CONST  and VARS
const KELVIN = 273;
//API Key
const key = "e72998cc037eea53b1e3006a17ace6ee";

// Check if browser suports Geolocation
if ('geolocation' in navigator) {
    navigator.geolocation.getCurrentPosition(setPosition, showError);
} else {
    notificationElement.style.display = "block";
    notificationElement.innerHTML = "<p>Browser doesn't Support Geolocation</p>";
}
//SET position
function setPosition(position) {
    let latitude = position.coords.latitude;
    let longitude = position.coords.longitude;

    getWeather(latitude, longitude);
}
// SHOW ERROR WHEN THERE IS AN ISSUE WITH GEOLOCATION SERVICE
function showError(error) {
    notificationElement.style.display = "block";
    notificationElement.innerHTML = `<p> ${error.message} </p>`;
}
// GET WEATHER FROM API PROVIDER
function getWeather(latitude, longitude) {
    let api = `http://api.openweathermap.org/data/2.5/weather?id=524901&lat=${latitude}&lon=${longitude}&appid=${key}&lang=pl`;

    console.log(api);

    fetch(api)
        .then(function (response) {
            let data = response.json();
            return data;
        })
        .then(function (data) {
            weather.temperature.value = Math.floor(data.main.temp - KELVIN);
            weather.description = data.weather[0].description;
            weather.iconId = data.weather[0].icon;
            weather.city = data.name;
            weather.country = data.sys.country;
            weather.pressure = data.main.pressure;
            weather.humidity = data.main.humidity;
            weather.windSpeed = data.wind.speed;
            weather.windDir = data.wind.deg;
            weather.sunrise = data.sys.sunrise;
            weather.sunset = data.sys.sunset;
        })
        .then(function () {
            displayWeather();
        })
}

// DISPLAY WEATHER TO UI
function displayWeather() {
    iconElement.innerHTML = `<img src = "icons/${weather.iconId}.png"/>`;
    tempElement.innerHTML = `${weather.temperature.value}&deg<span>C</span>`;
    descElement.innerHTML = weather.description;
    locationElement.innerHTML = `${weather.city}, ${weather.country}`;
    pressureElement.innerHTML = `<span>ciśnienie</span> ${weather.pressure} <span>hPa</span>`;
    humidityElement.innerHTML = `<span>wilgotność<span> ${weather.humidity} <span>%</span>`;
    windSpeedElement.innerHTML = `<span>prędkość wiatru</span> ${weather.windSpeed} <span>m/s</span>`;
    windDirElement.innerHTML = `<span>kierunek wiatru</span> ${weather.windDir} &deg`;


    let sunrise = `${weather.sunrise}`;
    const timezone = 1;

    const timesunrise = function () {
        let hours = Math.floor(sunrise / (60 * 60) % 24) + timezone; // czas zimowy  - do UTC + 1 hour
        hours = hours < 10 ? `0${hours}` : hours;
        let min = Math.floor((sunrise / 60) % 60);
        min = min < 10 ? `0${min}` : min;
        let sec = Math.floor((sunrise) % 60);
        sec = sec < 10 ? `0${sec}` : sec;

        spanH.textContent = hours;
        spanM.textContent = min;
        spanS.textContent = sec;
    };
    timesunrise();


    let sunset = `${weather.sunset}`;

    const timesunset = function () {
        let hours = Math.floor(sunset / (60 * 60) % 24) + timezone; // czas zimowy  - do UTC + 1 hour
        hours = hours < 10 ? `0${hours}` : hours;
        let min = Math.floor((sunset / 60) % 60);
        min = min < 10 ? `0${min}` : min;
        let sec = Math.floor((sunset) % 60);
        sec = sec < 10 ? `0${sec}` : sec;

        spanH1.textContent = hours;
        spanM1.textContent = min;
        spanS1.textContent = sec;
    };
    timesunset();

    //Conversion C to Fahrenheit
    function celsiusToFahrenheit(temperature) {
        return (temperature * 9 / 5) + 32;
    }
    // When the user clicks on the Temperature element
    tempElement.addEventListener("click", function () {
        if (weather.temperature.value === undefined) return;
        if (weather.temperature.unit == "celsius") {
            let fahrenheit = celsiusToFahrenheit(weather.temperature.value);
            fahrenheit = Math.floor(fahrenheit);
            tempElement.innerHTML = `${fahrenheit}&deg<span>F</span>`;
            weather.temperature.unit = "fahrenheit"
        } else {
            tempElement.innerHTML = `${weather.temperature.value}&deg<span>C</span>`;
            weather.temperature.unit = "celsius";
        }

    });
}
